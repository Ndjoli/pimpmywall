#!/bin/bash

#########################################################################
#Script Name	: omxplayer_full_page.sh
#Description	: Displaying a video
#Arg1           : if no arg the videos from the VIDEO_PATH are Dispaying
#		  else arg specifie the port number to receive stream
#Author       	: Elie N'djoli
#Email         	: elie.ndjolibohulu@heig-vd.ch
#########################################################################

trap "kill 0" EXIT
VIDEO_PATH="/home/pi/Videos"

arg=$1

#If they are no argument the videos of the folder Videos are displaying
if [ -z "$arg" ] 
then
	while :
        do
			for filename in $VIDEO_PATH/*.mp4; do
				omxplayer "$filename" &
				process_id=$!
				wait $process_id
            done
        done

#If the argument contain an ip address, omxplayer wait an rtsp stream 
elif [ $(echo "$arg" | grep -o "[.]" | wc -l) -eq 3 ] 
then
	omxplayer -b --live rtsp://$arg

#If the argument contain a port number, omxplayer wait an udp stream
elif [ "$arg" -ge 1 ] 
then
	omxplayer --timeout=999 -b 'udp://@:'$arg
else
	echo "paramètre incorrect"
fi

wait
