### Introduction

This project aims to easily manage a Raspberry pi to display some application. They are 3 different mode whose use is facilitated by a script shell. The first mode allow the user to display in full screen a web page. The second allow the user to display a movie in full screen and the third one allow the user to display a number to help his identification.

The implementation of these abstractions aims to be able to manage an entire fleet of raspberry pi and switch beetween each mode by ssh.

### Tutorial

This tutorial explain how prepare your Rasberry pi to set up this project.

1) Download and flash on a sd card the Raspbian Buster with desktop image. You can download it on the raspberrypi website https://www.raspberrypi.org/downloads/raspbian/

Then flash the sd card with this image.

For execute the step 2 to 6 including you need to let the SD card on your computer. 

2) Once the sd card is flashed, we will update the Wi-Fi informations. Open the file wpa_supplicant in the folder rootfs/etc/ with the root rights and add your network.

You can add severals networks in this file (you can use the example below as a guide). If you need more informations to add a network use the  linux manual (`man wpa_suppplicant`).

```
# Network HEIG-VD
network={
	ssid="HEIG-VD"
	#scan_ssid=1
	key_mgmt=WPA-EAP
	eap=PEAP
	identity="eliejaco.ndjolibo"
	password="xxxxxxxxxxx"
	#ca_cert="/etc/cert/ca.pem"
	#phase1="peaplabel=0"
	phase2="auth=MSCHAPV2"
}

# Network Home
network={
	ssid="Salt_5GHz_12345"
	scan_ssid=1
	key_mgmt=WPA-PSK
	psk="xxxxxxxx"
}

```

3)  SD card problems have been reported after prolonged usage because of a severals numbers of write on it. A majority of these writes are made in the temp and log files. To avoid this, it is necessary to write on the RAM  instead on the SD card.
For that, add on the fstab file (rootfs/etc/fstab)  the three lines below et allow 10Mo on RAM for each tmp and logs folder.

```bash
tmpfs /tmp tmpfs defaults,noatime,nosuid,size=10m 0 0
tmpfs /var/tmp tmpfs defaults,noatime,nosuid,size=10m 0 0
tmpfs /var/log tmpfs defaults,noatime,nosuid,mode=0755,size=10m 0 0
```

http://www.magdiblog.fr/divers/comment-prolonger-la-duree-de-vie-de-vos-cartes-sd-sur-raspberry-pi/

4) We will enable the ssh of the card. For that just add an empty file named ssh (`touch ssh` ) on the root folder.

5) Put the folders named "main_scripts" and "modes_scripts"  in rootfs/home/pi/

6) To close correctely the Raspberry pi, we need to stop all process before shutdown. To do that add the script "main_scripts/shutdown.sh" in the folder rootfs/lib/systemd/system-shutdown/

7) Now we can put the SD card on the raspberry pi and turn it on.  Then, connect you to the pi by ssh with the user pi (example : command `ssh pi@piaddress` in a terminal). The default password will be "raspberry".

8) Once connected to the terminal of the pi, enter the command  `sudo raspi-config`.

For limit the consuption of resources we will disable the desktop interface. For that follow that way : raspi-config => "Boot Options" => “Desktop / CLI”  => “Console Autologin”

We will disable the Overscan options in the section "Advanced Options" => "Overscan" for automatically adjust the screen size.

You can take the opportunity to update the password of the Raspberry pi, your location.

Then go out of the menu raspi-config.

9) We will update the list of packages and update the installed packages with the command  `sudo apt update && sudo apt upgrade` before to start new software installation.

10) We will need to imagemagick who modified or generate pictures. To install it, use the command `sudo apt install imagemagick`

11) We will need to xdotool who allow to simulate a mouse or a keyboard. The scripts use it to simulate F11 key who put the chromium-browser in fullscreen. To install it, use the command `sudo apt install xdotool`

12) If you want access to the pi by remote access you can install x11vnc. To install it, use the command `sudo apt-get install x11vnc`.

Start once x11vnc with the command  `x11vnc -forever -usepw -display :0` . This first start ask you a password. enter the password of your choice.

The command  `x11vnc -forever -usepw -display :0` will be add in cron table in the next step to start x11vnc when the raspberry pi is powered on.

13) So that the screen never goes to standby we need to install xscreensaver and configure it. To install it, use the command `sudo apt-get install xscreensaver` 

To configure xscreensaver, open the file nammed .xscreensaver in the folder /home/pi. Then edit the field timeout with the value "999:00:00". 

14) The video display mode need to use omxplayer. To install it use the command  `sudo apt-get install omxplayer` 

15) We need to install Openvpn to build a tunnel with the PMW Virtual Machine. To install it use the command  `sudo apt-get install openvpn` . 
To autostart the connexion on boot you need to use the client.ovpn generate by the server. Take the client file (client.ovpn) and rename it to client.conf and put the file client.conf on the folder /etc/openvpn. Then go on the file /etc/default/openvpn and uncomment the line `AUTOSTART="all"`

16) To start the system automatically we need to start X Windows and the default application Chromium-Browser

To start the X Windows System to make possible the graphical interaction, it is necessary to t to plan a task with crontab as root user. To open the config file use this command : `sudo crontab -e` , the add this line `@reboot (sleep 5;sudo Xorg) &` at the end of the file.

Then to start Chromium-Browser with his default page we need to plan a task with crontab as pi user. To open the config file use this command : `crontab -u pi -e` (this is important not to use a root user).
Subsequently, add this line  at the end of the crontab configuration file.

```bash
#Start the watchdog
@reboot (sleep 30;/home/pi/main_scripts/watchdog.sh) &
#Start Chormium-Browser with PimpMyWall his default page
@reboot (sleep 10;/home/pi/main_scripts/mode.sh 1) &
#Start xscreensaver
@reboot (sleep 20;DISPLAY=:0 xscreensaver) &
#Start xvnc
@reboot (sleep 30;x11vnc -forever -usepw -display :0)
```

17) Finally, to enable Librenms to supervising the state of the RBPI we need to install and configure the snmp agent. To install it, use the command `sudo apt install snmpd`.

Then,  add or uncomment this lines in the file /etc/snmp/snmpd.conf

```
agentAddress udp:161,udp6:[::1]:161
rocommunity public  default -V systemonly
```

Finally restart the service with the command `sudo /etc/init.d/snmpd restart`

18) To secure to Raspberry pi you can use the firewall ufw. For that use that  commands.

```
sudo ufw allow 22/tcp
sudo ufw allow 161
sudo ufw allow 23000
sudo ufw allow 8554
sudo ufw allow 5900
sudo ufw enable
```

ports utilities :

- 22 : SSH
- 161 : SNMP agent
- 23000 : UDP stream reception
- 8554 : RTSP stream reception
- 5900 : VNC

### Examples

Below are some examples for running the script "mode.sh" who are in /home/pi/main_scripts. Each use case are always in full screen.

#### Chromium-browser mode

<u>Opening a default page (Pimp My Wall) on Chromium-browser</u>

./mode 1

<u>Opening google.com on Chromium-browser</u> 

./mode 1 http://google.com 

<u>Note</u>

It's possible to interact with the browser  with a mouse and keyboard or with x11vnc

#### Videos mode

<u>Displaying from files</u>

./mode 2

<u>Displaying from UDP stream on port 1234</u>

./mode 2 1234

<u>Displaying from RTSP stream on serveur 192.1681.5</u>

./mode 2 192.1681.5

#### Identification mode

To identify a screen, you can ask him to display a number (5 in the example). For that use this command

./mode 3 5

### Trouble shooting

1) If you can't access to internet either than with an ip address. You need to set a plublic DNS on the RBPI. To do that go on the file "/etc/resolv.conf.head" and add this two line.

```
nameserver 1.1.1.1
nameserver 1.0.0.1
```

2) If you have some interference, blanking, or no display with the HDMI signal.
It's necessary to boost the signal. To do that Open the file /boot/config.txt and uncomment the line `config_hdmi_boost=4`

3) If you have ohters display problem show the documentation https://www.raspberrypi.org/documentation/configuration/config-txt/video.md who explain how configure a display.


### Source

Put the scripts output in a log file:  https://ops.tips/gists/redirect-all-outputs-of-a-bash-script-to-a-file/

Xorg : https://fr.wikipedia.org/wiki/X_Window_System

x11vnc : https://developingsoftware.com/installing-x11vnc-on-raspbian-and-connecting-to-raspberry-pi-with-vnc-viewer/

OpenVPN autostart: https://openvpn.net/community-resources/how-to/#configuring-openvpn-to-run-automatically-on-system-startup

DNS resolver : https://itsfoss.com/resolvconf-permanent-ubuntu/

HDMI hotplug : https://raspberrypi.stackexchange.com/questions/10371/no-hdmi-output-when-hdmi-cable-is-connected-after-power-supply

HDMI group and mode : <https://www.raspberrypi.org/documentation/configuration/config-txt/video.md> and <https://raspberrypi.stackexchange.com/questions/8985/forcing-resolution-on-display-0-when-hdmi-is-not-connected#8986>