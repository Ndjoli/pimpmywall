#!/bin/bash

#########################################################
#Script Name    : watchdog.sh
#Description    : Launch of the modes
#Arg1           : Mode selection (integer)
#Arg2           : Specific for each mode
#Author         : Elie N'djoli
#Email          : elie.ndjolibohulu@heig-vd.ch
#########################################################


LOG_FILE="/tmp/log_watchdog"
BASHRC_PATH="/home/pi/.bashrc"

#Redirection of the scripts output
exec 1>>$LOG_FILE
exec 2>>$LOG_FILE

while [ true ]
do
	#Get the hdmi_state with the command tvservice -s        
	hdmi_state=$(tvservice -s | cut -d ' ' -f2)

        #Get the second bit of the hdmi_state to know it the HDMI is connected
        hdmi_state_bit=$((($hdmi_state >> 1) & 0x1))

	#Check if a location is already set
        if [[ -n $(grep PMW_LOCATION= $BASHRC_PATH) ]] 
        then
                location=$(grep PMW_LOCATION= $BASHRC_PATH | cut -d '=' -f2)
                echo $PMW_LOCATION
        else
                location="default"
        fi

	#Get the current mode information
	current_mode=$(grep PMW_CURRENT_MODE= $BASHRC_PATH | cut -d '=' -f2)

	#Get the raspberry ip addrress
        ip=$(ip addr show tap0 | grep inet | head -n1 | tr -s ' ' | cut -d ' ' -f3 |cut -d '/' -f1)

	#If the VPN subnet(192.68.255.0/24) is available, A POST request is send to the piManager
        if [[ $ip == 192.168.255.* ]] 
        then
		#The default address of the piManager is 172.30.0.136
                curl -X POST http://172.30.0.136:8080/pi/$ip/$hdmi_state_bit/$current_mode?"$(echo "location=$location" | sed 's/ /%20/g' )"
                echo http://172.30.0.136:8080/pi/$ip/$hdmi_state_bit/$current_mode?"$(echo "location=$location" | sed 's/ /%20/g' )"
        fi

        sleep 10
done

