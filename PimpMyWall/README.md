## Introduction

This project will let you manage easily some Raspberry pi with your web browser.

## Run the application

These instructions will get you a copy of the project up and running on your computer.

### Prerequisites

- [`docker`](https://www.docker.com/) must be installed.
- [`docker-compose`](https://www.docker.com/) must be installed.
- [`git`](https://git-scm.com/) must be installed.

### Prepare the application

```sh
# Clone the repository
git clone https://gitlab.com/Ndjoli/pimpmywall.git

# Move to the cloned directory
cd pimpmywall/PimpMyWall

# Copy and edit the environment variables
mv .env.dist .env
```

### Generate an OpenVPN configuration file

This step will be used to generate a configuration file that will have to be deployed on each Rasoberry pi. To do that, follow the next steps.

- Initialize the `$OVPN_DATA` container that will hold the configuration files and certificates.  The container will prompt for a passphrase to protect the private key used by the newly generated certificate authority.

  ```bash
  cd src/openVPN
  
  docker-compose run --rm openvpn ovpn_genconfig -u udp://server_name -c -t -e duplicate-cn
  
  docker-compose run --rm openvpn ovpn_initpki
  ```

- Generate a client certificate without a passphrase

  ```bash
  export CLIENTNAME="your_client_name"
  # without a passphrase
  docker-compose run --rm openvpn easyrsa build-client-full $CLIENTNAME nopass
  ```

- Retrieve the client configuration with embedded certificates

  ```bash
  docker-compose run --rm openvpn ovpn_getclient $CLIENTNAME > $CLIENTNAME.ovpn
  ```

- Revoke a client certificate

```
# Keep the corresponding crt, key and req files.
docker-compose run --rm openvpn ovpn_revokeclient $CLIENTNAME
# Remove the corresponding crt, key and req files.
docker-compose run --rm openvpn ovpn_revokeclient $CLIENTNAME remove
```

Now you can take the CLIENTNAME.ovpn file and use it to create a tunnel between the raspberry pi and the server.

### Deploy the application

```sh
cd ../../

# Start the infrastructure and add route to the vpn subnet(by default: 192.168.255.0/24) to the openVPN container(by default: 172.30.0.135) 
sudo docker-compose up -d && sudo ip route add 192.168.255.0/24 via 172.30.0.135

# Optionally you can see the containers logs
sudo docker-compose logs -f
```

The application is now deployed:

- [localhost:8090](http://localhost:8090): Access the PiManager.
- [localhost:8091](http://localhost:8091): Access the LibreNMS.

### Access to the Raspberry pi with VNC

If you want take the mouse and/or keybord control on a Raspberry pi, you can proceed like this :
- Connected you on the VPN subnet with the command `sudo openvpn --config CLIENTNAME.ovpn`
- Use a VNC client and connect you with the Raspberry pi IP address