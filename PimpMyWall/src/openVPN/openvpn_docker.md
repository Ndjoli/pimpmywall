### Introduction

This tutorial is based on the repository  https://github.com/kylemanna/docker-openvpn. It explain how start the container openvpn to allow the Raspberry pi to connect to the server.

### Start the container

- Initialize the `$OVPN_DATA` container that will hold the configuration files and certificates.  The container will prompt for a passphrase to protect the private key used by the newly generated certificate authority.

  ```bash
  docker-compose run --rm openvpn ovpn_genconfig -u udp://213.55.240.77 -c -t -e duplicate-cn
  
  docker-compose run --rm openvpn ovpn_initpki
  ```
  
- Start OpenVPN server process

  ```bash
  docker-compose up -d openvpn
  ```

- Generate a client certificate without a passphrase

  ```bash
  export CLIENTNAME="your_client_name2"
  # with a passphrase (recommended)
  docker-compose run --rm openvpn easyrsa build-client-full $CLIENTNAME
  # without a passphrase (not recommended)
  docker-compose run --rm openvpn easyrsa build-client-full $CLIENTNAME nopass
  ```

- Retrieve the client configuration with embedded certificates

  ```bash
  docker-compose run --rm openvpn ovpn_getclient $CLIENTNAME > $CLIENTNAME.ovpn
  ```

- Revoke a client certificate

```
# Keep the corresponding crt, key and req files.
docker-compose run --rm openvpn ovpn_revokeclient $CLIENTNAME
# Remove the corresponding crt, key and req files.
docker-compose run --rm openvpn ovpn_revokeclient $CLIENTNAME remove
```

Now you can take the CLIENTNAME.ovpn file and use it to create a tunnel between the raspberry pi and the server.

For More informations please refer to https://github.com/kylemanna/docker-openvpn