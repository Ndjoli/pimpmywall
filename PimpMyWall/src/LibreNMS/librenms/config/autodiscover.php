<?php
$config['snmp']['community'][] = "public";
$config['nets'][] = '192.168.255.0/24';
$config['discovery_by_ip'] = true;
$config['allow_duplicate_sysName'] = true;
$config['discovery_modules']['discovery-arp'] = true;
