var SSH = require('simple-ssh');
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var tmp;

var arg;
var requestProjector;
const username = process.env.PI_USERNAME;
const password = process.env.PI_PASSWORD;
const projectorURL = 'http://admin:0000@169.254.1.1'; //Default adresse of the projectors

var listIndexRaspberry = [];  //List with the connected raspberry pis index
var raspberryHDMI = [];       //List of the hdmi state of the connected raspberry pis
var raspberryMode = [];       //List of the current mode used by the connected raspberry pis
var raspberryLocation = [];   //List of the raspberry pi's locations
var raspberryWatchDog = [];   //List the last time of raspbery pi's sign of life
var raspberryAddress = [];    //List of the RASPBERRY Pi's IP address

var listOfDeconectedPi = [];
var index;

//Static Ressources
app.use(express.static(__dirname + '/bower_components'));
app.use(express.static('public'));
app.use(express.static('src'));

//Principal Route
app.get('/', function (req, res, next) {
  res.render(__dirname + '/index.ejs');
});

//Socket.io function open when a web browser is connected
io.on('connection', function (client) {
  console.log('Client connected...');
  client.on('join', function (data) {

    //Function who send to the client the raspberry pi's update
    (async function () {
      console.log('envoie des informations');
      while(true){
        client.emit('piInformation', listIndexRaspberry, raspberryHDMI, raspberryMode, raspberryLocation);

        tmp = listOfDeconectedPi.length;
        for (var i = 0; i < tmp; i++){
          var index = listOfDeconectedPi.pop();
          console.log('piDisconnectInformation : ' + index);
          client.broadcast.emit('piDisconnectInformation', index);
        }
        await wait5Seconds();
      }
    })();
  });
});

//Watchdog function who check the raspberry pi activity
(async function () {
  var time = 20; //Time in secondes before considering a Raspbery pi disconnect
  while (true) {
    for (var i = 0; i < listIndexRaspberry.length; i++){
      index = listIndexRaspberry[i];
      if ((Date.now() - raspberryWatchDog[index]) / 1000 > time){
        listOfDeconectedPi.push(index);
        listIndexRaspberry.remove(index);
      }
    }
    await wait5Seconds();
  }
})();

//Route used by the raspberry pi to update their informations and notify the watchdog
app.post('/pi/:ipDevice/:hdmiState/:mode/', function (req, res, next) {
  var ipAddr = req.params.ipDevice; 
  var n = ipAddr.lastIndexOf(".") + 1; //position of the value of the 8 first bits of the ip address
  var index = ipAddr.substring(n, ipAddr.length);
  console.log('rbpi n° : ' + index);
  
  if (listIndexRaspberry.indexOf(index) == -1){
    listIndexRaspberry.push(index);
    raspberryAddress[index] = ipAddr;
  }

  raspberryHDMI[index] = req.params.hdmiState;
  raspberryMode[index] = req.params.mode;
  raspberryLocation[index] = req.query.location;
  raspberryWatchDog[index] = Date.now(); //Update of the watchdog time
  
  res.send();
});

//Route to change the mode of a device by specifying its IP address
app.post('/mode/:n/:ipDevice', function (req, res) {
  var ssh;

  console.log('mode : ' + req.params.n);
  console.log('adresse : ' + req.params.ipDevice);
  arg = null;

  //Check if they are a query in the URL
  if (typeof (req.query.url) != 'undefined') {
    arg = req.query.url;
  }
  else if (typeof (req.query.port) != 'undefined') {
    arg = req.query.port;
  }
  else if (typeof (req.query.id) != 'undefined') {
    arg = req.query.id;
  }
  else if (typeof (req.query.source) != 'undefined') {
    arg = req.query.source;
  }
  else if (typeof (req.query.delay) != 'undefined') {
    arg = req.query.delay;
  }

  //If they are an argument, it's send to the mode script
  if (arg !== null) {
    if (req.params.ipDevice != 'all'){
      ssh = new SSH({
        host: req.params.ipDevice,
        user: username,
        pass: password
      });

      ssh.exec('./main_scripts/mode.sh ' + req.params.n + ' ' + arg, {
        out: console.log.bind(console)
      })
        .exec('exit', {
          out: console.log.bind(console)
        })
        .start();
    }
    else{
      console.log("all mode avec arg");
      for (var i = 0; i < listIndexRaspberry.length; i++) {
        ssh = new SSH({
          host: raspberryAddress[listIndexRaspberry[i]],
          user: username,
          pass: password
        });
        ssh.exec('./main_scripts/mode.sh ' + req.params.n + ' ' + arg, {
          out: console.log.bind(console)
        })
          .exec('exit', {
            out: console.log.bind(console)
          })
          .start();
      }
    }
  }
  //Else the mode script is used without argument
  else {
    if (req.params.ipDevice != 'all') {
      ssh = new SSH({
        host: req.params.ipDevice,
        user: username,
        pass: password
      });
      ssh.exec('./main_scripts/mode.sh ' + req.params.n, {
        out: console.log.bind(console)
      })
        .exec('exit', {
          out: console.log.bind(console)
        })
        .start();
        
      ssh.on('error', function (err) {
        console.log('SSH error');
        console.log(err);
        ssh.end();
      });
    }
    else{
      console.log("all mode sans arg")
      for (var i = 0; i < listIndexRaspberry.length; i++) {
        ssh = new SSH({
          host: raspberryAddress[listIndexRaspberry[i]],
          user: username,
          pass: password
        });
        ssh.exec('./main_scripts/mode.sh ' + req.params.n, {
          out: console.log.bind(console)
        })
          .exec('exit', {
            out: console.log.bind(console)
          })
          .start();

        ssh.on('error', function (err) {
          console.log('SSH error');
          console.log(err);
          ssh.end();
        });
      }
    }
  }
  res.send()
});

app.post('/piIdentification/', function (req, res, next) {
  var identificationMode = 3;
  var ssh;
  console.log("Identification all");
  for (var i = 0; i < listIndexRaspberry.length; i++) {
    ssh = new SSH({
      host: raspberryAddress[listIndexRaspberry[i]],
      user: username,
      pass: password
    });
    ssh.exec('./main_scripts/mode.sh ' + identificationMode + ' ' + listIndexRaspberry[i], {
      out: console.log.bind(console)
    })
      .exec('exit', {
        out: console.log.bind(console)
      })
      .start();
  }
  res.send()
});

//Route to shutdown a device by specifying its IP address
app.delete('/shutdown/:ipDevice/', function (req, res) {
  var ssh;
  if (req.params.ipDevice != 'all'){
    ssh = new SSH({
      host: req.params.ipDevice,
      user: username,
      pass: password
    });
    console.log('shutdown ' + req.params.ipDevice);

    ssh.exec('sudo shutdown -h now ', {
      out: console.log.bind(console),
    }).start();

    ssh.on('error', function (err) {
      console.log('Connection broken');
      console.log(err);
      ssh.end();
    });
  }
  else{
    console.log('shutdown all');
    for(var i = 0; i < listIndexRaspberry.length; i++){
      ssh = new SSH({
        host: raspberryAddress[listIndexRaspberry[i]],
        user: username,
        pass: password
      });
      ssh.exec('sudo shutdown -h now ', {
        out: console.log.bind(console),
      }).start();

      ssh.on('error', function (err) {
        console.log('Connection broken');
        console.log(err);
        ssh.end();
      });
    }
  }
  res.send()
});

//Route to reboot a device by specifying its IP address
app.post('/reboot/:ipDevice/', function (req, res) {
  var ssh;
  if (req.params.ipDevice != 'all') {
    //New ssh connection
    ssh = new SSH({
      host: req.params.ipDevice,
      user: username,
      pass: password
    });
    console.log('reboot ' + req.params.ipDevice);

    ssh.exec('sudo reboot ', {
      out: console.log.bind(console),
    }).start();

    ssh.on('error', function (err) {
      console.log('Connection broken');
      console.log(err);
      ssh.end();
    });
  }
  else{
    console.log("reboot all");
    for (var i = 0; i < listIndexRaspberry.length; i++) {
      console.log("address : " + raspberryAddress[listIndexRaspberry[i]]);
      ssh = new SSH({
        host: raspberryAddress[listIndexRaspberry[i]],
        user: username,
        pass: password
      });

      ssh.exec('sudo reboot ', {
        out: console.log.bind(console),
      }).start();

      ssh.on('error', function (err) {
        console.log('Connection broken');
        console.log(err);
        ssh.end();
      });
    }
  }
  res.send()
});

//Route to get a json list of all connected devices
app.get('/devices', function (req, res) {
  var array = [];
  var obj;
  for (var i = 0; i < listIndexRaspberry.length; i++) {
    var index = listIndexRaspberry[i];
    obj = new Object();
    obj.addressIp = raspberryAddress[index];
    obj.hdmiState = raspberryHDMI[index]; 
    obj.mode = raspberryMode[index]; 
    obj.location = raspberryLocation[index]; 
    array[i] = obj;
  }
  var json = JSON.stringify(array);
  console.log(json);
  res.write(json);
  res.send()
});

//Route to attribute a new location to a raspberry pi
app.post('/setLocation/:ipDevice/', function (req, res, next) {
  console.log('begin set location');
  var ssh = new SSH({
    host: req.params.ipDevice,
    user: username,
    pass: password
  });
  ssh.exec('variable_location=$(grep PMW_LOCATION= .bashrc) && sed -i \'s/\'\"$variable_location\"\'/PMW_LOCATION="' + req.query.location + '"/g\' .bashrc' +
    ' || echo PMW_LOCATION="' + req.query.location + '" >> .bashrc', {
    out: function (stdout) {
      console.log(stdout);
    }
    })
    .exec('exit', {
      out: console.log.bind(console)
    }).start();

  ssh.on('error', function (err) {
    console.log('SSH error');
    console.log(err);
    ssh.end();
  });

  res.write('commande ok');
  res.send()
});

//Route to manage the Raspberry pi's projector
app.post('/projector/:command/:ipDevice/', function (req, res) {

  var ssh;

  if (req.params.command == 'poweron') {
    requestProjector = projectorURL + '/protect/execPwr.cgi?PWRCHG=1';
  }
  else if (req.params.command == 'poweroff') {
    requestProjector = projectorURL + '/protect/execPwr.cgi?PWRCHG=2';
  }
  else if (req.params.command == 'muteon') {
    requestProjector = projectorURL + '/protect/execMute.cgi?MUTESEL=1';
  }
  else if (req.params.command == 'muteoff') {
    requestProjector = projectorURL + '/protect/execMute.cgi?MUTESEL=0';
  }
  else if (req.params.command == 'freezeon') {
    requestProjector = projectorURL + '/protect/execFree.cgi?FREESEL=1';
  }
  else if (req.params.command == 'freezeoff') {
    requestProjector = projectorURL + '/protect/execFree.cgi?FREESEL=0';
  }

  if (req.params.ipDevice != 'all') {
    ssh = new SSH({
      host: req.params.ipDevice,
      user: username,
      pass: password
    });

    //https://stackoverflow.com/questions/9691367/how-do-i-request-a-file-but-not-save-it-with-wget
    ssh.exec('wget -qO- ' + requestProjector + ' &> /dev/null', {
        out: console.log.bind(console)
      })
      .exec('exit', {
        out: console.log.bind(console)
      })
      .start();

    ssh.on('error', function (err) {
      console.log('SSH error');
      console.log(err);
      ssh.end();
    });
  }
  else{
    console.log("projector all");
    for (var i = 0; i < listIndexRaspberry.length; i++) {
      ssh = new SSH({
        host: raspberryAddress[listIndexRaspberry[i]],
        user: username,
        pass: password
      });
      ssh.exec('wget -qO- ' + requestProjector + ' &> /dev/null', {
        out: console.log.bind(console)
      })
        .exec('exit', {
          out: console.log.bind(console)
        })
        .start();

      ssh.on('error', function (err) {
        console.log('SSH error');
        console.log(err);
        ssh.end();
      });
    }
  }

  res.send()
});

server.listen(8080);

//Wait function inspiring by the web site 
//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function
function wait5Seconds() {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve('resolved');
    }, 5000);
  });
}

//https://stackoverflow.com/questions/3954438/how-to-remove-item-from-array-by-value
Array.prototype.remove = function () {
  var what, a = arguments, L = a.length, ax;
  while (L && this.length) {
    what = a[--L];
    while ((ax = this.indexOf(what)) !== -1) {
      this.splice(ax, 1);
    }
  }
  return this;
};