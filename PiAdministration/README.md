### Introduction
To facilitate the sending of commands and files update at all the raspberry pi you can use this tools.

### Prerequisites

You need to be connected on the VPN subnet. To do that use the .ovpn config file generate with the openVPN docker container and start it with the following command

```bash
sudo openvpn --config configFile.ovpn
```

Note : if you can't access to internet after you connected on the VPN subnet. edit your /etc/resolv.conf and change the nameserver local adress with the address of a public DNS server

### Send a command to all the raspberry pi
To send a command to all the Raspberry pi you can use this following command
```sh
#Example
./commandForAll.sh "sudo reboot"
```

### Update the files of all Raspberry pi
by starting the script `./deployUpdatesForAll.sh` you can update a lot of files on every connected Raspberry pi.
For that you juste need to edit the content of the following folders.

- Folder "Videos" : the videos content in this folder are duplicate on the Raspberry pi and are accessible by the mode SD card
- Folder "boot" : this folder content the config.txt file. By editing this file you can change the monitor configuration.
- Folder "main_scripts" : you can deploy an update or a new script on the folder main_script of the Raspberry pi
- Folder "modes_scripts" : you can deploy an update or a new script on the folder modes_script of the Raspberry pi
- Folder "crontab" : this folder contain the cron table

:warning: It is important to be aware that every file deleted from his folder will be deleted on the Raspberry pi during the deployment.
