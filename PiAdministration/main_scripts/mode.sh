#!/bin/bash

#########################################################
#Script Name	: mode.sh
#Description	: Launch of the modes
#Arg1           : Mode selection (integer)
#Arg2           : Specific for each mode
#Author       	: Elie N'djoli
#Email         	: elie.ndjolibohulu@heig-vd.ch
#########################################################

SCRIPTS_PATH="/home/pi/modes_scripts"
BASHRC_PATH="/home/pi/.bashrc"
LOG_FILE="/tmp/log_mods"

#Redirection of the scripts output
exec 1>>$LOG_FILE
exec 2>>$LOG_FILE

#Close scripts that are running
for script in $SCRIPTS_PATH/*.sh; do
	nb_process=$(ps aux | grep $script | wc -l)
	process_pid=$(ps aux | grep $script | head -n1 | tr -s ' ' | cut -d ' ' -f 2)
	if [[ $process_pid -ge 1 && $nb_process -gt 1 ]]
	then
		kill $process_pid
	fi
done

if [ -n $1 ] 
then
	#Mode 1: Chromium-browser
	#Arg   : Refer. to $SCRIPTS_PATH/chromium_full_page.sh
	if [ $1 -eq 1 ]
	then
		if [ -n "$2" ]
		then 
			$SCRIPTS_PATH/chromium_full_page.sh $2 &

			#Update the environment variable
			variable=$(grep PMW_CURRENT_MODE= $BASHRC_PATH) &&
			sed -i 's/'"$variable"'/PMW_CURRENT_MODE=Chromium_address_custom/g' $BASHRC_PATH ||
			echo "PMW_CURRENT_MODE=Chromium_address_custom" >> $BASHRC_PATH
		else
			$SCRIPTS_PATH/chromium_full_page.sh &
			
			#Update the environment variable
			variable=$(grep PMW_CURRENT_MODE= $BASHRC_PATH) &&
			sed -i 's/'"$variable"'/PMW_CURRENT_MODE=PimpMyWall/g' $BASHRC_PATH ||
			echo "PMW_CURRENT_MODE=PimpMyWall" >> $BASHRC_PATH
		fi
	#Mode 2: Video displayer
	#Arg   : Refer. to $SCRIPTS_PATH/omxplayer_full_page.sh
	elif [ $1 -eq 2 ]
	then
		if [ -n "$2" ]
		then
			$SCRIPTS_PATH/omxplayer_full_page.sh $2 &

			#Update the environment variable
                        variable=$(grep PMW_CURRENT_MODE= $BASHRC_PATH) &&
                        sed -i 's/'"$variable"'/PMW_CURRENT_MODE=stream_reception/g' $BASHRC_PATH ||
                        echo "PMW_CURRENT_MODE=stream_reception" >> $BASHRC_PATH

		else
			$SCRIPTS_PATH/omxplayer_full_page.sh &
			#Update the environment variable
                        variable=$(grep PMW_CURRENT_MODE= $BASHRC_PATH) &&
                        sed -i 's/'"$variable"'/PMW_CURRENT_MODE=SD_card_videos/g' $BASHRC_PATH ||
                        echo "PMW_CURRENT_MODE=SD_card_videos" >> $BASHRC_PATH

		fi
	#Mode 3: Identification
	#Arg   : Refer. to $SCRIPTS_PATH/screen_identification.sh
	elif [ $1 -eq 3 ]
	then
		if [ -n "$2" ]
		then
			$SCRIPTS_PATH/screen_identification.sh $2 &

			#Update the environment variable
			variable=$(grep PMW_CURRENT_MODE= $BASHRC_PATH) &&
			sed -i 's/'"$variable"'/PMW_CURRENT_MODE=Identification/g' $BASHRC_PATH ||
			echo "PMW_CURRENT_MODE=Identification" >> $BASHRC_PATH
		else
			$SCRIPTS_PATH/screen_identification.sh 999 &

			#Update the environment variable
			variable=$(grep PMW_CURRENT_MODE= $BASHRC_PATH) &&
			sed -i 's/'"$variable"'/PMW_CURRENT_MODE=Identification_default/g' $BASHRC_PATH ||
			echo "PMW_CURRENT_MODE=Identification_default" >> $BASHRC_PATH
		fi
	#Mode 4: Sponsor
	#Arg   : Refer. to $SCRIPTS_PATH/images_kiosk.sh
	elif [ $1 -eq 4 ]
	then
		if [ -n "$2" ]
		then
			$SCRIPTS_PATH/images_kiosk.sh $2 &

			#Update the environment variable
			variable=$(grep PMW_CURRENT_MODE= $BASHRC_PATH) &&
			sed -i 's/'"$variable"'/PMW_CURRENT_MODE=image_kiosk/g' $BASHRC_PATH ||
			echo "PMW_CURRENT_MODE=image_kiosk" >> $BASHRC_PATH
		fi
	fi
fi
