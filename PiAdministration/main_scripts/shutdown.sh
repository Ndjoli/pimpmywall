#!/bin/bash

#########################################################
#Script Name	: shutdown.sh
#Description	: Launch of the modes
#Arg1           : Mode selection (integer)
#Arg2           : Specific for each mode
#Author       	: Elie N'djoli
#Email         	: elie.ndjolibohulu@heig-vd.ch
#########################################################

SCRIPTS_PATH="/home/pi/modes_scripts"
process_to_kill=(xorg xscreensaver openvpn)

#Redirection of the scripts output
exec 1>/dev/null
exec 2>/dev/null

#Close scripts that are running
for script in $SCRIPTS_PATH/*.sh; do
	nb_process=$(ps aux | grep script | wc -l)
	process_pid=$(ps aux | grep script | head -n1 | tr -s ' ' | cut -d ' ' -f 2)
	if [[ $process_pid -ge 1 && $nb_process -gt 1 ]]
	then
		kill $process_pid
	fi
done

#Close process of the process_to_kill array
for i in "${process_to_kill[@]}"; do
	nb_process=$(ps aux | grep $i | wc -l)
	process_pid=$(ps aux | grep $i | head -n1 | tr -s ' ' | cut -d ' ' -f 2)
	if [[ $process_pid -ge 1 && $nb_process -gt 1 ]]
	then
		kill $process_pid
	fi
done

