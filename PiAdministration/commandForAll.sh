#!/bin/bash

###################################################################
#Script Name	: commandForAll.sh
#Description	: Send a command to all Raspberry pi
#Arg1           : Command to send
#Author       	: Elie N'djoli
#Email         	: elie.ndjolibohulu@heig-vd.ch
#Comment        : sshpass and jq should be installed on your system
###################################################################

#Password of the raspberry
export SSHPASS=raspberry

serveurAddress='167.99.142.103:8090'
LOG_FILE="/tmp/log_commandForAll"

#Generate the address IP list
curl -s http://$serveurAddress/devices | jq -r '.[].addressIp' > listAddress.txt

command=$1

#exec 2>>$LOG_FILE

if [[ -n $command ]] 
then
	while read address
	do
		echo $address $command
		sshpass -e ssh -o StrictHostKeyChecking=no pi@$address $command </dev/null

	done < listAddress.txt
else
	echo "Please enter a command"
fi


#Ressources
#https://stackoverflow.com/questions/1955505/parsing-json-with-unix-tools
#https://stackoverflow.com/questions/34543829/jq-cannot-index-array-with-string
#https://stackoverflow.com/questions/1521462/looping-through-the-content-of-a-file-in-bash
#https://serverfault.com/questions/318474/how-to-pass-password-to-scp-command-used-in-bash-script
