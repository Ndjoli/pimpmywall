#!/bin/bash

###########################################################
#Script Name	: screen_identification.sh                                                                                             
#Description	: Diplaying a number to help identification                                                                               
#Arg1           : Number to display between 1 and 999                                                                                  
#Author       	: Elie N'djoli                                                
#Email         	: elie.ndjolibohulu@heig-vd.ch                                          
###########################################################

trap "kill 0" EXIT

#The number is displaying if he's between 0 and 999
if [[ -n "$1" && $1 -ge 0 && $1 -le 999 ]]
then
	resolution=$(tvservice -s | cut -d ',' -f2 | cut -d ' ' -f2)
	DISPLAY=:0 display -size $resolution -gravity center -backdrop label:$1
fi

wait
