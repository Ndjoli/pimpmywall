#!/bin/bash

#########################################################################
#Script Name	: chromium_full_page.sh
#Description	: Displaying a web browser
#Arg1           : if no arg Pimp My Wall is open
#				  else arg specifie an URL to open
#Author       	: Elie N'djoli
#Email         	: elie.ndjolibohulu@heig-vd.ch
#########################################################################

trap "kill 0" EXIT

#Address to access to Pimp My Wall display mode
pimpMyWallAdress='http://167.99.142.103:8082/'

#Start the fullscreen command with a delay of 10 seconds and put the mouse in the display corner
(sleep 15; DISPLAY=:0 xdotool key F11; DISPLAY=:0 xdotool mousemove 10000 10000) &

#If no url specicied a basic page is started
if [ -n "$1" ] 
then
	#If they are a parameter, this one is open in the browser
	DISPLAY=:0 chromium-browser --window-size=1920,1080 --disable-infobars --start-maximized $1
else
	#Else the browser is opened with the PimpMyWall ip address
	DISPLAY=:0 chromium-browser --window-size=1920,1080 --disable-infobars --start-maximized $pimpMyWallAdress
fi

wait
