#!/bin/bash

###########################################################
#Script Name    : image_kiosk.sh
#Description    : Displaying image kiosk
#Arg1           : Display time of each image [s]
#Author         : Elie N'djoli
#Email          : elie.ndjolibohulu@heig-vd.ch
###########################################################

trap "kill 0" EXIT
IMAGE_PATH=/home/pi/Images_kiosk/

if [[ -n "$1" ]]
then
    	#Get the screen resolution
	resolution=$(tvservice -s | cut -d ',' -f2 | cut -d ' ' -f2)

	#Generate a gif loop with the images to display
	convert -resize $resolution -delay $(($1*100)) /home/pi/Images_kiosk/* -loop 9999 /tmp/image_kiosk.gif

	#Display the gif on the screen
	DISPLAY=:0 display -size $resolution -gravity center -backdrop /tmp/image_kiosk.gif
fi

wait
