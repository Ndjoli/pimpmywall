#!/bin/bash

###################################################################
#Script Name	: deployUpdateForAll.sh
#Description	: Deploy files to all Raspberry pi
#Author       	: Elie N'djoli
#Email         	: elie.ndjolibohulu@heig-vd.ch
#Comment        : sshpass and jq should be installed on your system
###################################################################

#Password of the raspberry
export SSHPASS=raspberry

#Username of the raspberry
pi_userName=pi

LOG_FILE="/tmp/log_deployUpdt"
serverAddress='167.99.142.103:8090'

#Redirection of the scripts output
#exec 1>>$LOG_FILE
exec 2>>$LOG_FILE

#Generate the address IP list
curl -s http://$serverAddress/devices | jq -r '.[].addressIp' > listAddress.txt

while read address
do
	if [ -d main_scripts ]; then
		echo main_scripts $address
		rsync -v -r --rsh="sshpass -e ssh -o StrictHostKeyChecking=no -l $pi_userName" main_scripts $address:/home/pi/ --delete
		sshpass -e ssh $pi_userName@$address "chmod +x /home/$pi_userName/main_scripts/*" </dev/null
	fi

	if [ -d modes_scripts ]; then
		echo modes_scripts $address
		rsync -v -r --rsh="sshpass -e ssh -o StrictHostKeyChecking=no -l $pi_userName" modes_scripts $address:/home/pi/ --delete
		sshpass -e ssh $pi_userName@$address "chmod +x /home/$pi_userName/modes_scripts/*" </dev/null
        fi

	if [ -d Videos ]; then
		echo Videos $address
		rsync -v -r --rsh="sshpass -e ssh -o StrictHostKeyChecking=no -l $pi_userName" Videos $address:/home/pi/ --delete
        fi

	if [ -d crontab ]; then
		if [ ! -f "crontabNoneRoot" ]; then
			rsync -v -r --rsh="sshpass -e ssh -o StrictHostKeyChecking=no -l $pi_userName" crontab/crontabNoneRoot $address:/tmp/
			sshpass -e ssh $pi_userName@$address "crontab /tmp/crontabNoneRoot" </dev/null
		fi

		if [ ! -f "crontabRoot" ]; then
			rsync -v -r --rsh="sshpass -e ssh -o StrictHostKeyChecking=no -l $pi_userName" crontab/crontabRoot $address:/tmp/
			sshpass -e ssh $pi_userName@$address "sudo crontab /tmp/crontabRoot" </dev/null
		fi
	fi

	if [ -d wpa_supplicant ]; then
		if [ ! -f "wpa_supplicant.conf" ]; then
			rsync -v -r --rsh="sshpass -e ssh -o StrictHostKeyChecking=no -l $pi_userName" wpa_supplicant/wpa_supplicant.conf $address:/tmp/
			sshpass -e ssh $pi_userName@$address "sudo cp /tmp/wpa_supplicant.conf /etc/wpa_supplicant" </dev/null
		fi
	fi

	if [ -d boot ]; then
		if [ ! -f "config.txt" ]; then
			echo "config.txt" $address
			rsync -v -r --rsh="sshpass -e ssh -o StrictHostKeyChecking=no -l $pi_userName" boot/config.txt $address:/tmp/
			sshpass -e ssh $pi_userName@$address "sudo cp /tmp/config.txt /boot/config.txt" </dev/null
		fi
	fi

	if [ -d Images_kiosk ]; then
		rsync -v -r --rsh="sshpass -e ssh -o StrictHostKeyChecking=no -l $pi_userName" Images_kiosk $address:/home/pi/ --delete
	fi

done < listAddress.txt

#Ressources used
#https://stackoverflow.com/questions/1955505/parsing-json-with-unix-tools
#https://stackoverflow.com/questions/34543829/jq-cannot-index-array-with-string
#https://stackoverflow.com/questions/1521462/looping-through-the-content-of-a-file-in-bash
#https://serverfault.com/questions/318474/how-to-pass-password-to-scp-command-used-in-bash-script
#https://serverfault.com/questions/330503/scp-without-known-hosts-check
#https://www.cyberciti.biz/faq/noninteractive-shell-script-ssh-password-provider/
