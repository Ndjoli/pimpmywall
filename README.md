### Introduction
This project contain the tools to deploy and manage a network of Raspberry pi.

### Prerequisites

- [`docker`](https://www.docker.com/) must be installed.
- [`docker-compose`](https://www.docker.com/) must be installed.
- [`git`](https://git-scm.com/) must be installed.

### Prepare the application

```sh
# Clone the repository
git clone https://gitlab.com/Ndjoli/pimpmywall.git
```

### Procedure to follow
1) To deploy the system you can start by follow the readme of the folder "PimpMyWall".
2) Once you have generate the vpn configuration (file.ovpn). You can follow the readme of the folder "RaspberryPi" who explain you the manipulations to do to generate the Raspberry pi image.
3) Then you can duplicate the generated images of the step two for all your Raspberry pi. Now the system is operational.
4) Finally, if you want modify or deploy a new configuration on all the Raspberry pi you can follow the readme of the folder "PiAdministration".