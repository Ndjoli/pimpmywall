#!/bin/bash

#########################################################################
#Script Name	: simulation.sh
#Description	: This script make a simulation of 4 raspberry pi
#Author       	: Elie N'djoli
#Email         	: elie.ndjolibohulu@heig-vd.ch
#########################################################################

i=0
if [[ -n "$serverName" ]] 
then
    while [ true ]
    do
    	for index in {2..5}
    	do
    		curl -X POST http://$serverName/pi/192.168.255.$index/$(($index % 2))/$i?location=station_test_$index
    		echo http://$serverName/pi/192.168.255.$index/$(($index % 2))/$i?location=station_test_$index
    	done
    	((i++))
    	sleep 5
    done
else
    echo "Please add the serverName environment variable"
fi
