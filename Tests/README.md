### Introduction
This directory contain some tools to test the fiability of the piManager.

### Prerequisites
you need to add an environment variable with the name of the piManagerServer
```sh
export serverName=localhost:8090

```

### Run the tests
```sh
# Simulate the presence of 4 raspberry pi
./simulation.sh

# Simulate the presence of 254 Raspberry pi
./stress_test.sh

```