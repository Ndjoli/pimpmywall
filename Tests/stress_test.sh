#!/bin/bash

#########################################################################
#Script Name	: stres_test.sh
#Description	: This test simulates the presence of 254 raspberry pi at
#                 the same time
#Author       	: Elie N'djoli
#Email         	: elie.ndjolibohulu@heig-vd.ch
#########################################################################

i=0
if [[ -n "$serverName" ]] 
then
    while [ true ]
    do
    	for index in {2..254}
    	do
    		curl -X POST http://$serverName/pi/192.168.255.$index/$(($index % 2))/$i?location=station_test_$index
    		echo http://$serverName/pi/192.168.255.$index/$(($index % 2))/$i?location=station_test_$index
    	done
    	((i++))
    done
else
    echo "Please add the serverName environment variable"
fi
